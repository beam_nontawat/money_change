package com.example.moneychange.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.moneychange.MainActivity;
import com.example.moneychange.R;

public class Output extends Fragment {
    private MainActivity activity;
    TextView money, money1000, money500, money100, money50, money20, money10, money5, money2, money1;
    private  int[] Data = new int[2];

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_output,container,false);
        money = (TextView) view.findViewById(R.id.money);
        money1000 = (TextView) view.findViewById(R.id.money1000);
        money500 = (TextView) view.findViewById(R.id.money500);
        money100 = (TextView) view.findViewById(R.id.money100);
        money50 = (TextView) view.findViewById(R.id.money50);
        money20 = (TextView) view.findViewById(R.id.money20);
        money10 = (TextView) view.findViewById(R.id.money10);
        money5 = (TextView) view.findViewById(R.id.money5);
        money2 = (TextView) view.findViewById(R.id.money2);
        money1 = (TextView) view.findViewById(R.id.money1);

        Data = activity.CallData();
        Calculator_Money(Data[1] ,Data[0]);
        return view;
    }
    public void Calculator_Money(int Price, int Receive){
        int Money = Receive - Price;
        money.setText(String.valueOf(Money));
        if(Money >= 1000){
            money1000.setText(String.valueOf(Money/1000));
            Money %= 1000;
        }
        if(Money >= 500){
            money500.setText(String.valueOf(Money/500));
            Money %= 500;
        }
        if(Money >= 100){
            money100.setText(String.valueOf(Money/100));
            Money %= 100;
        }
        if(Money >= 50){
            money50.setText(String.valueOf(Money/50));
            Money %= 50;
        }
        if(Money >= 20){
            money20.setText(String.valueOf(Money/20));
            Money %= 20;
        }
        if(Money >= 10){
            money10.setText(String.valueOf(Money/10));
            Money %= 10;
        }
        if(Money >= 5){
            money5.setText(String.valueOf(Money/5));
            Money %= 5;
        }
        if(Money >= 2){
            money2.setText(String.valueOf(Money/2));
            Money %= 2;
        }
        money1.setText(String.valueOf(Money));

    }

    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }


}
