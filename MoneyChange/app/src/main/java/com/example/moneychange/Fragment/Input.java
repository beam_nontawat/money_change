package com.example.moneychange.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.moneychange.MainActivity;
import com.example.moneychange.R;


public class Input extends Fragment {
    private MainActivity activity;
    int[] Data = new int[2];
    EditText price, receive;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input,container,false);
        Button button = (Button) view.findViewById(R.id.calculator);
        price = (EditText) view.findViewById(R.id.price);
        receive = (EditText) view.findViewById(R.id.receive);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calculator();
            }
        });
        return view;
    }


    public void Calculator(){
        if(!validate_price() | !validate_receive()){
            return;
        }
        if(Data[0] < Data[1]){
            Toast.makeText(getContext(),"ราคาสินค้ามากกว่าจำนวนเงิน", Toast.LENGTH_SHORT).show();
            return;
        }
        activity.KeepData(Data);
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,new Output())
                .addToBackStack(null)
                .commit();
    }

    private boolean validate_receive(){
        String Receive = receive.getText().toString().trim();
        if(Receive.isEmpty()){
            receive.setError("กรุณากรอกข้อมูล");
            return false;
        }else {
            receive.setError(null);
            Data[0] = Integer.parseInt(Receive);
            return true;
        }
    }

    private boolean validate_price(){
        String Price = price.getText().toString().trim();
        if(Price.isEmpty()){
            price.setError("กรุณากรอกข้อมูล");
            return false;
        }else {
            price.setError(null);
            Data[1] = Integer.parseInt(Price);
            return true;
        }
    }

    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }



}
