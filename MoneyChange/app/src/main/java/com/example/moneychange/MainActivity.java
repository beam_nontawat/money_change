package com.example.moneychange;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import com.example.moneychange.Fragment.Input;
import com.example.moneychange.Fragment.Output;

public class MainActivity extends AppCompatActivity {
    int[] Data = new int[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new Input())
                    .commit();
        }

    }

    public void Back_Calculator(View view){
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new Input())
                .commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public int[] CallData(){
        return Data;
    }
    public void KeepData(int[] data){
        Data = data;
    }


}
